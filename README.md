# README #

### What is this repository for? ###
 SneakersAPP - Basic app with loading screen and feed screen that shows list of products


### Design/Implementation decisions ###
- Error logging - i'm just doing basic print statements, given time constraints. In a real project, I'd use some kind of cloud logging (swifty beaver or graylog for example)


### What wasn't done ###

I was really pushed to get it all done in the 4 hours, so I had to focus more on the "back end" stuff, the APIs, models etc.

1) Loading screen
-  I just didn't have time to do it

2) Feed screen
I got the API stuff done to get the filters and the method that fetches the products accepts parameters, but I didn't implement the filter picker.
I'd probably do a modal with tableview or something like that, but deprioritised this item due toe time constraints.

The list of products will show 2 products per row in iPhone portrait, 3 on iPhone landscape and 4 on iPad (every orientation).  -  didn't spend any time on this


## Optional: 
I did do tests for the LystAPI for parsing of models etc. I would definitely add more, given time

The UI is absolute basic, I didn't have time for the transitions or animations pieces

3rd party frameworks - I setup the project for cocoapods, but didn't end up using any in the end. Guven from 


