//
//  ProductViewModel.swift
//  Sneakers
//
//  Created by Andy on 17/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import Foundation
import UIKit
import LystAPI

public struct ProductViewModel {

    let product: Product

    init(product: Product, imageCache: NSCache<AnyObject, AnyObject>) {
        self.product = product
    }

    var title: String {
        return product.title
    }

    var price: String {
        return product.price
    }

    func configure(productCell: ProductCell, imageCache: NSCache<AnyObject, AnyObject>) {
        productCell.titleLabel.text = title
        productCell.priceLabel.text = price
        productCell.productImageView.loadImageUsingUrlString(urlString: product.image, imageCache: imageCache)
    }

}
