//
//  ProductCell.swift
//  Sneakers
//
//  Created by Andy on 17/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import UIKit

class ProductCell: UICollectionViewCell {

        //MARK:- Outlets
        @IBOutlet var titleLabel: UILabel!
        @IBOutlet var priceLabel: UILabel!
        @IBOutlet var productImageView: UIImageView!

}
