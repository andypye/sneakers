//
//  ViewController.swift
//  Sneakers
//
//  Created by Andy on 13/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import UIKit
import LystAPI

class ProductListViewController: UIViewController {

    // MARK:- Outlets
    @IBOutlet weak var collectionView: UICollectionView!

    // MARK:- Private Properties
    private var products: [Product] = []
    private var filters: [Filter] = []
    private let activityIndicator = UIActivityIndicatorView()
    private let imageCache = NSCache<AnyObject, AnyObject>()

    private enum collectionViewCells: String {

        case product

        var reuseIdentifier: String {
            switch self {
            case .product:
                return "ProductCell"
            }
        }
    }

    //MARK: Private Computed properties
    private var collectionViewHidden: Bool {
        return self.products.count == 0
    }

    private var collectionViewHiddenMessage: String? {
        if collectionViewHidden {
            return "We're really sorry about this, all the users appear to have gone home. Please try again later."
        }
        else {
            return nil
        }
    }

    private func configureDisplay(hideTableView: Bool, userMessage: String?) {

        collectionView.isHidden = collectionViewHidden
        //userMessageView.isHidden = !tableViewHidden
        if collectionViewHidden == false {
            collectionView.reloadData()
        }
        if let userMesage = userMessage {
    //        userMessageTextView.text = userMesage
        }
        else {
      //      userMessageTextView.text = ""
        }
        self.activityIndicator.stopAnimating()
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        setupActivityIndicator()
        repopulateCollectionView()
        repopulatefilterOptions()
        // Do any additional setup after loading the view, typically from a nib.
    }

    // MARK: Private Methods
    private func setupActivityIndicator() {
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.style = .gray
        view.addSubview(activityIndicator)
        activityIndicator.isHidden = false
    }

    private func repopulateCollectionView() {
        activityIndicator.startAnimating()
        LystAPI.getProducts(gender: "M", category: "boots") { fetchedProducts, success, userMessage in
            print("fetchedProducts.count \(fetchedProducts.count)")
            DispatchQueue.main.async {
                self.products = fetchedProducts
                if success {
                    self.configureDisplay(hideTableView: self.collectionViewHidden, userMessage: self.collectionViewHiddenMessage)
                } else {
                    self.configureDisplay(hideTableView: true, userMessage: "Unable to fetch products. We're really sorry about this, please try again later.")
                }
            }
        }
    }

    private func repopulatefilterOptions() {
        //activityIndicator.startAnimating()
        LystAPI.getSneakerFilters(gender:  "M") { fetchedFilters, success, userMessage in
        //getProducts(gender: "M", category: "boots") { fetchedFilters, success, userMessage in
            print("fetchedFilters.count \(fetchedFilters.count)")
            DispatchQueue.main.async {
                self.filters = fetchedFilters
                if success {
                    //self.configureDisplay(hideTableView: self.tableViewHidden, userMessage: self.tableViewHiddenMessage)
                } else {
                    //self.configureDisplay(hideTableView: true, userMessage: "Unable to fetch filters. We're really sorry about this, please try again later.")
                }
            }
        }
    }

}

// MARK: UICollectionViewDelegate
extension ProductListViewController: UICollectionViewDelegate {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

}

// MARK: UICollectionViewDataSource
extension ProductListViewController: UICollectionViewDataSource {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return products.count
    }

    func collectionView(_ collectionView: UICollectionView,
                                 cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let productViewModel = ProductViewModel(product: products[indexPath.row], imageCache: imageCache)
        let productCell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionViewCells.product.reuseIdentifier,
                                                      for: indexPath) as! ProductCell
        productViewModel.configure(productCell: productCell, imageCache: imageCache)
        return productCell
    }

}
