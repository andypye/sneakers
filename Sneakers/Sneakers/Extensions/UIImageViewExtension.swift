//
//  UIImageViewExtension.swift
//  Sneakers
//
//  Created by Andy on 17/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import UIKit

extension UIImageView {

    func loadImageUsingUrlString(urlString: String, imageCache: NSCache<AnyObject, AnyObject>) {

        if let imageFromCache = imageCache.object(forKey: urlString as AnyObject) as? UIImage {
            self.image = imageFromCache
            return
        }

        if let url = URL(string: urlString) {
            URLSession.shared.dataTask(with: url) { (data, responses, error) in
                if let unwrappedError = error {
                    print("ERROR: Image download failed: \(unwrappedError)")
                    return
                }
                guard let unwrappedData = data else {
                    print("ERROR: No data when getting image, so we leave imageview untouched")
                    return
                }
                // If all ok, then update the image, must be on the main thread
                DispatchQueue.main.async {
                    if let newImage = UIImage(data: unwrappedData) {
                        self.image = newImage
                        imageCache.setObject(newImage, forKey: urlString as AnyObject)
                    }
                    else {
                        print("ERROR: Image was nil, so we leave imageview untouched")
                    }
                }
                }.resume()
        }
        else {
            print("ERROR: Invalid URL")
        }
    }
}
