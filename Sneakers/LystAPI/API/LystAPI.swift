//
//  LystAPI.swift
//  LystAPI
//
//  Created by Andy on 14/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import Foundation

public struct LystAPI {

    enum HttpSuccess {
        case success
        case fail
    }

    static let productType = "Shoes"
    static let filterType = "category"

    public typealias getProductCompletionHandler = (([Product], _ success: Bool, _ message: String) -> ())
    public typealias getFilterCompletionHandler = (([Filter], _ success: Bool, _ message: String) -> ())

    //MARK: - Static methods
    static public func getProducts(gender: String, category: String, completion: @escaping getProductCompletionHandler) {

        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        guard let request = APIEndpoint.feed(gender: gender, category: category).request else {
            print("ERROR: the request could not be created")
            return
        }

        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard let statusCode = getStatusCode(response: response) else {
                completion([], false, "Status code was nil, error: \(String(describing: error))")
                return
            }
            guard validateRequestStatus(statusCode: statusCode) == .success else {
                completion([], false, "Unable to fetch Feed, status code \(statusCode)")
                return
            }
            if let error = error {
                // Failure
                self.handleFailure(response: response, error: error)
                completion([], false, "Unable to fetch Feed, please try again")
            } else {
                // Success
                let jsonParser = JSONParser()
                guard let products = jsonParser.parseProductJSON(data: data, response: response) else {
                    completion([], false, "Unable to fetch Feed, please try again")
                    return
                }
                completion(products, true, "All fetch feeds was all good")
                }
        })
        task.resume()
    }

    //MARK: - Static methods
    static public func getSneakerFilters(gender: String, completion: @escaping getFilterCompletionHandler) {
        getFilters(gender: gender, productType: self.productType, filterType: self.filterType, completion: completion)

    }

     static private func getFilters (gender: String, productType: String, filterType: String, completion: @escaping getFilterCompletionHandler) {

        let sessionConfig = URLSessionConfiguration.default
        let session = URLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        guard let request = APIEndpoint.filter(gender: gender, productType: productType, filterType: filterType).request else {
            print("ERROR: the request could not be created")
            return
        }

        let task = session.dataTask(with: request, completionHandler: { (data: Data?, response: URLResponse?, error: Error?) -> Void in
            guard let statusCode = getStatusCode(response: response) else {
                completion([], false, "Status code was nil, error: \(String(describing: error))")
                return
            }
            guard validateRequestStatus(statusCode: statusCode) == .success else {
                completion([], false, "Unable to fetch Filters, status code \(statusCode)")
                return
            }
            if let error = error {
                // Failure
                self.handleFailure(response: response, error: error)
                completion([], false, "Unable to fetch Filters, please try again")
            } else {
                // Success
                let jsonParser = JSONParser()
                guard let filters = jsonParser.parseFilterJSON(data: data, response: response) else {
                    completion([], false, "Unable to fetch Filters, please try again")
                    return
                }
                completion(filters, true, "Fetch Filters was all good")
            }
        })
        task.resume()
    }

   
    static private func getStatusCode(response: URLResponse?) -> Int? {

        guard let httpUrlResponse = response as? HTTPURLResponse else { return nil }
        return httpUrlResponse.statusCode
    }

    static private func getStatusCodeString(response: URLResponse?) -> String {

        if let getStatusCode = getStatusCode(response: response) {
            return String(getStatusCode)
        }
        else {
            return "Unknown"
        }
    }

    static private func validateRequestStatus(statusCode: Int?) -> HttpSuccess {
        // TODO: Improve this or find 3rd party
        // 5xx Server errors
        // 4xx Client errors
        // 3xx Redirection
        // 2xx Success
        if let statusCode = statusCode, statusCode < 300 {
            return HttpSuccess.success
        }
        else {
            return HttpSuccess.fail
        }
    }

    static private func handleFailure(response: URLResponse?, error: Error) {
        print("ERROR: Fetch of Feed failed wth code:\(getStatusCodeString(response: response))\n\nerror: \(error)")
    }


}
