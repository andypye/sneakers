//
//  JSONParser.swift
//  LystAPI
//
//  Created by Andy Pye on 15/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import Foundation

struct JSONParser {

    //MARK: - Static methods
    func parseProductJSON(data: Data?, response: URLResponse?) -> [Product]? {

        guard let jsonData = data else {
            print("ERROR: guard let jsonData failed")
            return nil
        }

        do {
            let jsonDecoder = JSONDecoder()
            jsonDecoder.dateDecodingStrategy = .iso8601
            let feedData = try jsonDecoder.decode(ProductData.self, from: jsonData)
            return feedData.products
        }
        catch let error {
            print("ERROR: JSON decode failed error: \(error)")
            return nil
        }
    }

    func parseFilterJSON(data: Data?, response: URLResponse?) -> [Filter]? {

        guard let jsonData = data else {
            print("ERROR: guard let jsonData failed")
            return nil
        }

        do {
            let jsonDecoder = JSONDecoder()
            jsonDecoder.dateDecodingStrategy = .iso8601
            let filterData = try jsonDecoder.decode(FilterData.self, from: jsonData)
            return filterData.filters
        }
        catch let error {
            print("ERROR: JSON decode failed error: \(error)")
            return nil
        }
    }
}
