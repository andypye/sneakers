//
//  EndPoint.swift
//  LystAPI
//
//  Created by Andy on 14/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import Foundation

enum APIEndpoint {

    case filter(gender: String, productType: String, filterType: String)
    case feed(gender: String, category: String)

    var scheme: String {
        return "https"
    }

    var host: String {
        return "api.lyst.com"
    }

    var baseURL: String {
        return "/rest_api/"
    }

    var path: String {
        switch self {
        case .filter:
            return self.baseURL + "components/filter_options"
        case .feed:
            return self.baseURL + "components/feeds"
        }
    }

    var httpMethod: String {
        switch self {
        case .filter:
            return "GET"
        case .feed:
            return "GET"
        }
    }

    var contentType: String {
        return "application/json"
    }

    var urlQueryItems: [URLQueryItem] {
        switch self {

        case let .filter(gender, productType, filterType):
            let queryItems = [URLQueryItem(name: "pre_gender", value: gender),
                              URLQueryItem(name: "pre_product_type", value: productType),
                              URLQueryItem(name: "filter_type", value: filterType)
            ]
            return queryItems

        case let .feed(gender, category):
            let queryItems = [URLQueryItem(name: "pre_gender", value: gender),
                              URLQueryItem(name: "pre_category", value: category)]
            return queryItems
        }
    }

    var request: URLRequest? {
        var urlComponents = URLComponents()
        urlComponents.scheme = self.scheme
        urlComponents.host =  self.host
        urlComponents.path = self.path
        urlComponents.queryItems = self.urlQueryItems

        guard let url = urlComponents.url else {
            print("ERROR: Could not create User URL from components")
            return nil
        }

        var request = URLRequest(url: url)
        request.httpMethod = self.httpMethod
        var headers = request.allHTTPHeaderFields ?? [:]
        headers["Content-Type"] = self.contentType
        request.allHTTPHeaderFields = headers
        return request
    }

}
