//
//  Filter.swift
//  LystAPI
//
//  Created by Andy on 14/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import Foundation

public struct Filter: Decodable {

    //MARK:- Properties
    let value: String
    let label: String
    let available: Bool
    let active: Bool
    let type: String
    let imageUrl: String?
    let preFilter: Bool

    private enum FilterCodingKey: String, CodingKey {
        case value = "value"
        case label = "label"
        case available = "available"
        case active = "active"
        case type = "type"
        case imageUrl = "image_url"
        case preFilter = "pre_filter"
    }

    //MARK:- Methods
    public init(from decoder: Decoder) throws {
        do {
            let filterContainer = try decoder.container(keyedBy: FilterCodingKey.self)

            value = try filterContainer.decode(String.self, forKey: .value)
            label = try filterContainer.decode(String.self, forKey: .label)
            available = try filterContainer.decode(Bool.self, forKey: .available)
            active = try filterContainer.decode(Bool.self, forKey: .active)
            type = try filterContainer.decode(String.self, forKey: .type)
            imageUrl = try filterContainer.decode(String?.self, forKey: .imageUrl)
            preFilter = try filterContainer.decode(Bool.self, forKey: .preFilter)
        }
        catch let error {
            // Log the error then propagate
            print("ERROR: Failure for a product record \(error)")
            throw error
        }
    }

}
