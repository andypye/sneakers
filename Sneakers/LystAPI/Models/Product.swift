//
//  Product.swift
//  LystAPI
//
//  Created by Andy on 14/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import Foundation

public struct Product: Decodable {

    //MARK:- Properties
    public let designerSlug: String // Note, public let can't be changed after it's been set, by the init method
    let designerName: String
    let slug: String
    public let title: String
    public let image: String
    public let price: String
    let onSale: Bool
    let salePrice: String
    let discount: String
    let isPinned: Bool
    let inStock: Bool
    let isIcon: Bool
    let freeShipping: Bool
    let dateAdded: Date

    private enum ProductCodingKey: String, CodingKey {
        case designerSlug = "designer_slug"
        case designerName = "designer_name"
        case slug = "slug"
        case title = "title"
        case image = "image"
        case price = "price"
        case onSale = "on_sale"
        case salePrice = "sale_price"
        case discount = "discount"
        case isPinned = "is_pinned"
        case inStock = "in_stock"
        case isIcon = "is_icon"
        case freeShipping = "free_shipping"
        case dateAdded = "date_added"
    }

    //MARK:- Methods
    public init(from decoder: Decoder) throws {
        do {
            let productContainer = try decoder.container(keyedBy: ProductCodingKey.self)

            designerSlug = try productContainer.decode(String.self, forKey: .designerSlug)
            designerName = try productContainer.decode(String.self, forKey: .designerName)
            slug = try productContainer.decode(String.self, forKey: .slug)
            title = try productContainer.decode(String.self, forKey: .title)
            image = try productContainer.decode(String.self, forKey: .image)
            price = try productContainer.decode(String.self, forKey: .price)
            onSale = try productContainer.decode(Bool.self, forKey: .onSale)
            salePrice = try productContainer.decode(String.self, forKey: .salePrice)
            discount = try productContainer.decode(String.self, forKey: .discount)
            isPinned = try productContainer.decode(Bool.self, forKey: .isPinned)
            inStock = try productContainer.decode(Bool.self, forKey: .inStock)
            isIcon = try productContainer.decode(Bool.self, forKey: .isIcon)
            freeShipping = try productContainer.decode(Bool.self, forKey: .freeShipping)
            dateAdded = try productContainer.decode(Date.self, forKey: .dateAdded)
        }
        catch let error {
            // Log the error then propagate
            print("ERROR: Failure for a product record \(error)")
            throw error
        }
    }

}
