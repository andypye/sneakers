//
//  FilterData.swift
//  LystAPI
//
//  Created by Andy Pye on 16/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import Foundation

struct FilterData: Decodable {

    let filters: [Filter]

    enum CodingKeys: String, CodingKey {
        case filters
    }

    init(from decoder: Decoder) throws {
        let FiltersContainer = try decoder.container(keyedBy: CodingKeys.self)
        do {
            filters =  try FiltersContainer.decode([Filter].self, forKey: .filters)
        }
        catch let error {
            // Log the error then propagate
            print("ERROR: Filter Data JSON decode failed: \(error)")
            throw error
        }
    }

}
