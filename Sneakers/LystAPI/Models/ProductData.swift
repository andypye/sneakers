//
//  ProductData.swift
//  LystAPI
//
//  Created by Andy on 14/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import Foundation

struct ProductData: Decodable {

    let products: [Product]

    enum CodingKeys: String, CodingKey {
        case products
    }

    init(from decoder: Decoder) throws {
        let productsContainer = try decoder.container(keyedBy: CodingKeys.self)
        do {
            products =  try productsContainer.decode([Product].self, forKey: .products)
        }
        catch let error {
            // Log the error then propagate
            print("ERROR: Product Data JSON decode failed: \(error)")
            throw error
        }
    }

}
