//
//  FilterTests.swift
//  LystAPITests
//
//  Created by Andy Pye on 15/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import XCTest
@testable import LystAPI

class FilterTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFilterOK() {
        let json = """
     {
            "value": "boots",
            "label": "Boots",
            "available": true,
            "active": false,
            "type": "category",
            "image_url": null,
            "pre_filter": false
        }
    """.data(using: .utf8)!

        do {
            let jsonDecoder = JSONDecoder()
            jsonDecoder.dateDecodingStrategy = .iso8601
            let filter = try jsonDecoder.decode(Filter.self, from: json)

            XCTAssertEqual(filter.value, "boots")
            XCTAssertEqual(filter.label, "Boots")
            XCTAssertEqual(filter.available, true)
            XCTAssertFalse(filter.active)
            XCTAssertEqual(filter.type, "category")
            XCTAssertNil(filter.imageUrl)
            XCTAssertFalse(filter.preFilter)
        }
        catch let error {
            XCTFail("Product test errored: \(error)")
        }
    }

    func testFilterThrows() {
        let json = """
    {
            "valueX": "boots",
            "label": "Boots",
            "available": true,
            "active": false,
            "type": "category",
            "image_url": null,
            "pre_filter": false
    }
    """.data(using: .utf8)!

        let jsonDecoder = JSONDecoder()
        XCTAssertThrowsError(try jsonDecoder.decode(Filter.self, from: json))
    }

}
