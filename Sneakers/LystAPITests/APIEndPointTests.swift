//
//  EndPointTests.swift
//  LystAPITests
//
//  Created by Andy on 14/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//

import XCTest
@testable import LystAPI

class APIEndPointTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testScheme() {
        let filterEndPoint = APIEndpoint.filter(gender: "M", productType: "boots", filterType: "category")
        let feedsEndPoint = APIEndpoint.feed(gender: "M", category: "boots")
        XCTAssertEqual(filterEndPoint.scheme, "https")
        XCTAssertEqual(feedsEndPoint.scheme, "https")
    }

    func testHost() {
        let filterEndPoint = APIEndpoint.filter(gender: "M", productType: "boots", filterType: "category")
        let feedEndPoint = APIEndpoint.feed(gender: "M", category: "boots")
        XCTAssertEqual(filterEndPoint.host, "api.lyst.com")
        XCTAssertEqual(feedEndPoint.host, "api.lyst.com")
    }

    func testHttpMethod() {
        let filterEndPoint = APIEndpoint.filter(gender: "M", productType: "boots", filterType: "category")
        let feedEndPoint = APIEndpoint.feed(gender: "M", category: "boots")
        XCTAssertEqual(filterEndPoint.httpMethod, "GET")
        XCTAssertEqual(feedEndPoint.httpMethod, "GET")
    }

    func testContentType() {
        let filterEndPoint = APIEndpoint.filter(gender: "M", productType: "boots", filterType: "category")
        let feedEndPoint = APIEndpoint.feed(gender: "M", category: "boots")
        XCTAssertEqual(filterEndPoint.contentType, "application/json")
        XCTAssertEqual(feedEndPoint.contentType, "application/json")
    }

    func testFilterPath() {
        let filterEndPoint = APIEndpoint.filter(gender: "M", productType: "boots", filterType: "category")
        XCTAssertEqual(filterEndPoint.path, "/rest_api/components/filter_options")
    }

    func testFeedPath() {
        let feedEndPoint = APIEndpoint.feed(gender: "M", category: "boots")
        XCTAssertEqual(feedEndPoint.path, "/rest_api/components/feeds")
    }

    func testFeedQueryItems() {
        let feedEndPoint = APIEndpoint.feed(gender: "M", category: "boots")

        let genderQueryItem = URLQueryItem(name: "pre_gender", value: "M")
        let categoryQueryItem = URLQueryItem(name: "pre_category", value: "boots")

        XCTAssertEqual(feedEndPoint.urlQueryItems,[genderQueryItem,categoryQueryItem])
    }

    func testFilterQueryItems() {
        let filterEndPoint = APIEndpoint.filter(gender: "M", productType: "boots", filterType: "category")

        let genderQueryItem = URLQueryItem(name: "pre_gender", value: "M")
        let productTypeQueryItem = URLQueryItem(name: "pre_product_type", value: "boots")
        let filterTypeQueryItem = URLQueryItem(name: "filter_type", value: "category")

        XCTAssertEqual(filterEndPoint.urlQueryItems,[genderQueryItem,productTypeQueryItem,filterTypeQueryItem])
    }
    
}
