//
//  ProductTests.swift
//  LystAPITests
//
//  Created by Andy on 14/10/2018.
//  Copyright © 2018 Pyrotechnic Apps Ltd. All rights reserved.
//
import XCTest
@testable import LystAPI

class ProductTests: XCTestCase {

    func testProductOK() {
        let json = """
    {
        "designer_slug": "talbots",
        "designer_name": "Talbots",
        "slug": "talbots-iona-kitten-heel-ankle-boots-haircalf:YZTVKYR",
        "title": "Iona Kitten Heel Ankle Boots-haircalf",
        "image": "https://cdnc.lystit.com/photos/talbots/c5d30a36/talbots-Classic-Leopard-Iona-Kitten-Heel-Ankle-Boots-haircalf.jpeg",
        "price": "£166",
        "on_sale": true,
        "sale_price": "£117",
        "discount": "30%",
        "is_pinned": false,
        "in_stock": true,
        "is_icon": false,
        "free_shipping": false,
        "date_added": "2018-09-21T14:41:57Z"
    }
    """.data(using: .utf8)!

        do {
            let jsonDecoder = JSONDecoder()
            jsonDecoder.dateDecodingStrategy = .iso8601
            let product = try jsonDecoder.decode(Product.self, from: json)

            XCTAssertEqual(product.designerSlug, "talbots")
            XCTAssertEqual(product.designerName, "Talbots")
            XCTAssertEqual(product.slug, "talbots-iona-kitten-heel-ankle-boots-haircalf:YZTVKYR")
            XCTAssertEqual(product.title, "Iona Kitten Heel Ankle Boots-haircalf")
            XCTAssertEqual(product.image, "https://cdnc.lystit.com/photos/talbots/c5d30a36/talbots-Classic-Leopard-Iona-Kitten-Heel-Ankle-Boots-haircalf.jpeg")
            XCTAssertEqual(product.price, "£166")
            XCTAssertEqual(product.onSale, true)
            XCTAssertEqual(product.salePrice, "£117")
            XCTAssertEqual(product.discount, "30%")
            XCTAssertEqual(product.isPinned, false)
            XCTAssertEqual(product.inStock, true)
            XCTAssertEqual(product.isIcon, false)
            XCTAssertEqual(product.freeShipping, false)
            let dateFormatter = ISO8601DateFormatter()
            let dateAddedTestDate = dateFormatter.date(from: "2018-09-21T14:41:57Z")
            XCTAssertEqual(product.dateAdded, dateAddedTestDate)
        }
        catch let error {
            XCTFail("Product test errored: \(error)")
        }
    }

    func testProductThrows() {
        let json = """
    {
        "designer_slug": "talbots",
        "designer_nameX": "Talbots",
        "slug": "talbots-iona-kitten-heel-ankle-boots-haircalf:YZTVKYR",
        "title": "Iona Kitten Heel Ankle Boots-haircalf",
        "image": "https://cdnc.lystit.com/photos/talbots/c5d30a36/talbots-Classic-Leopard-Iona-Kitten-Heel-Ankle-Boots-haircalf.jpeg",
        "price": "£166",
        "on_sale": true,
        "sale_price": "£117",
        "discount": "30%",
        "is_pinned": false,
        "in_stock": true,
        "is_icon": false,
        "free_shipping": false,
        "date_added": "2018-09-21T14:41:57Z"
    }
    """.data(using: .utf8)!

        let jsonDecoder = JSONDecoder()
        jsonDecoder.dateDecodingStrategy = .iso8601
        XCTAssertThrowsError(try jsonDecoder.decode(Product.self, from: json))
    }

}
